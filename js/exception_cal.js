/**
 * @file
 * Exception-cal widget logic.
 */

(function ($) {
  "use strict";

  $.fn.exception_cal = function (options) {
    // Private vars.
    // Wrapper for calendar-markup.
    var $calendarWrapper;
    var actualYear;
    // Elements where the json-encoded date will be stored and initial parsed from.
    var $datesContainer;
    var $profilesContainer;
    // For area calculations.
    var lastSelectedTD;
    // Save if we add or remove with the last click.
    var lastSelectionMode;

    var initialProfiles;
    var initialDates;

    // This ist hier die collection.
    var $context = this;

    var singleDates = {};
    var profiles = {};
    var profileDates = {};

    var $allActualTDs;

    // Defaults.
    var settings = $.extend({
      input_singles: ".ohe-cal-dates",
      input_profiles: ".ohe-cal-profiles",
      // The outer wrapper for markup of the calendar.
      cal_markup_cont_class: "ohe-cal-wrapper",
      nav_classes: "form-submit",
      weekdays_abbr: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      // Mo.
      first_dow: 1,
      // Columns for calendar view.
      cols: 12,
      // Allow profile usage.
      profiles: true
    }, options);

    init();

    function init() {
      // Profile usage.
      if (settings.profiles) {
        $profilesContainer = $(settings.input_profiles, $context);
        initialProfiles = JSON.parse($profilesContainer.val());
        // Defaults and check if there any profiles.
        initialProfiles.inUse = initialProfiles.inUse || [];
        initialProfiles.notInUse = initialProfiles.notInUse || [];
        if (initialProfiles.inUse.length === 0 && initialProfiles.notInUse.length === 0) {
          settings.profiles = false;
        }
        profiles = initialProfiles;
        // Set profileDates.
        recalculateProfileDates();
      }

      // Container fields.
      $datesContainer = $(settings.input_singles, $context);

      // Parse init data.
      initialDates = JSON.parse($datesContainer.val());
      if ($.isEmptyObject(initialDates)) {
        // Necessary if an empty array comes here.
        initialDates = {};
      }

      // Fill dates list with initial values.
      singleDates = initialDates;

      // Generate wrapper for calendar markup.
      $calendarWrapper = $('<div class="' + settings.cal_markup_cont_class + '"></div>').insertAfter($datesContainer);

      initCalendar(new Date().getFullYear());
    }

    function recalculateProfileDates() {
      // Get dates from profiles marked as inUse and aggregate.
      profileDates = [];
      if (!$.isEmptyObject(profiles) && !$.isEmptyObject(profiles.inUse)) {

        // Foreach profile.
        for (var i = 0; i < profiles.inUse.length; i += 1) {
          // Single profile dates.
          var dates = profiles.inUse[i].dates;

          for (var year in dates) {
            if (dates.hasOwnProperty(year)) {
              profileDates[year] = profileDates[year] || {};
              for (var month in dates[year]) {
                if (dates[year].hasOwnProperty(month)) {
                  profileDates[year][month] = profileDates[year][month] || [];
                  // Duplicates are not a problem here.
                  profileDates[year][month] = profileDates[year][month].concat(dates[year][month]);
                }
              }
            }
          }

        }
      }
    }

    // Called initial and on year change.
    function initCalendar(year) {
      // Init vars.
      var selectionModus;
      actualYear = year;

      // Generate new markup.
      $calendarWrapper.empty().append(generateYear(year));
      // ohe-cal-inactive are inactive days(do not belong to this month).
      $allActualTDs = $('td:not(.ohe-cal-inactive)', $calendarWrapper);

      // Activate form.
      if (settings.profiles) {
        setProfileFormHandling();
      }

      // Helper for right click. Change right click to ctrl-leftclick. Thats easier to process.
      $('td:not(.ohe-cal-inactive)', $context).bind("contextmenu", function(e){
        e.preventDefault();
        var newEvent = jQuery.Event("click");
        newEvent.ctrlKey = true;
        $(this).trigger(newEvent);
      });

      // Init single selection handling.
      $('td:not(.ohe-cal-inactive)', $context).click(function (e) {
        var $clickedTD = $(this);
        e.preventDefault();

        // Add or remove? if td has class ohe-cal-selected its already selected.
        selectionModus = !$clickedTD.hasClass('ohe-cal-selected');

        // Shift/ctrl/alt(let the user decide) click - area selection-> do not update last.
        if (e.shiftKey || e.ctrlKey || e.altKey || e.which === 3) {

          // For every month thats in question.
          for (var i = lastSelectedTD.data('month'); i <= $(this).data('month'); i++) {
            // Get all tds and check.
            $('table[data-month="' + i + '"] td:not(.ohe-cal-inactive)', $context).each(function () {
              var $tdToCheck = $(this);
              // Restriktionen:im letzten monat-> $tdToCheck<klicked_td && im ersten monat-> $tdToCheck>last_sel_td  der rest ist umformungsspass.
              if (($tdToCheck.data('month') !== $clickedTD.data('month') || $tdToCheck.data('day') <= $clickedTD.data('day')) && ($tdToCheck.data('month') !== lastSelectedTD.data('month') || $tdToCheck.data('day') > lastSelectedTD.data('day'))) {
                if (lastSelectionMode) {
                  $tdToCheck.addClass('ohe-cal-selected');
                }
                else {
                  $tdToCheck.removeClass('ohe-cal-selected');
                }
              }
            });
          }
        }
        else {

          // Single date add/remove.
          if (selectionModus) {
            $clickedTD.addClass('ohe-cal-selected');
          }
          else {
            $clickedTD.removeClass('ohe-cal-selected');
          }
          // Remember single selection as possible startpoint for area-selection.
          lastSelectedTD = $clickedTD;
          lastSelectionMode = selectionModus;

        }
        updateSingleDates();
      });

      // Navigation.
      $('.ohe-cal-nav', $context).click(function (e) {
        e.preventDefault();
        initCalendar(parseInt($(this).data('year')));
      });
    }

    function updateSingleDates() {
      // Müssen wir überhaupt die daten als array of date führen?-> ja selbstverständlich, wo sonst sollen die daten der anderen jahre aufbewahrt werden-> evtl muss nicht date sein-> datenstruktur evtl besser.
      // (Re)set.
      singleDates[actualYear] = {};
      $allActualTDs.filter('.ohe-cal-selected').each(function () {
        var td = $(this);
        // singleDates[actualYear].push(new Date(actualYear, td.data('month'), td.data('day')));.
        singleDates[actualYear][td.data('month')] = singleDates[actualYear][td.data('month')] || [];
        singleDates[actualYear][td.data('month')].push(td.data('day'));
      });
      $datesContainer.val(JSON.stringify(singleDates));
    }

    function generateProfileOptionsForm() {
      var inputElements = '';
      var checked;

      for (var usagetype in profiles) {
        if (profiles.hasOwnProperty(usagetype)) {
          inputElements = inputElements + '<ul>';
          if (usagetype === "inUse") {
            checked = ' checked';
          }
          else {
            checked = '';
          }
          var usagetypeProfiles = profiles[usagetype];
          for (var i = 0; i < usagetypeProfiles.length; i += 1) {
            inputElements = inputElements + '<li><label><input type="checkbox" name="profile"' + checked + ' value="' + usagetypeProfiles[i].id + '">' + usagetypeProfiles[i].name + '</label></li>';
          }
          inputElements = inputElements + '</ul>';
        }
      }

      // Generate checkbox formular markup.
      var form = '<input type="button" class="ohe-cal-prof ohe-cal-pop ';
      form += settings.nav_classes;
      form += '" value="Profile">';
      form += '<div class="ohe-cal-popup"><form class="ohe-cal-profile-form">';
      form += inputElements;
      form += '<input type="button" name="profile-submit" value="Profile übernehmen" class="form-submit ohe-cal-profile-submit">';
      form += '</form></div>';

      return form;
    }

    function setProfileFormHandling() {
      $('.ohe-cal-prof.ohe-cal-pop', $context).click(function (e) {
        e.preventDefault();
        $('.ohe-cal-popup', $context).toggle();
      });
      $('.ohe-cal-profile-form .ohe-cal-profile-submit', $context).click(function (e) {
        var active = [];

        e.preventDefault();
        $('.ohe-cal-popup', $context).hide();
        var formFields = $('.ohe-cal-profile-form', $context).serializeArray();
        $(formFields).each(function (i, field) {
          if (field.name === "profile") {
            active.push(field.value);
          }
        });
        rebuildProfiles(active);

        // Rewrite calendar.
        initCalendar(actualYear);
      });
    }

    function rebuildProfiles(active) {
      var newProfiles = {
        inUse: [],
        notInUse: []
      };

      // Cycle all profiles.
      for (var usagetype in profiles) {
        if (profiles.hasOwnProperty(usagetype)) {
          var usagetypeProfiles = profiles[usagetype];
          for (var i = 0; i < usagetypeProfiles.length; i += 1) {
            if (active.indexOf(usagetypeProfiles[i].id) >= 0) {
              newProfiles.inUse.push(usagetypeProfiles[i]);
            }
            else {
              newProfiles.notInUse.push(usagetypeProfiles[i]);
            }
          }
        }
      }

      profiles = newProfiles;
      recalculateProfileDates();
      // Write back to input.
      $profilesContainer.val(JSON.stringify(newProfiles));
    }

    // Generates markup for year.
    function generateYear(year) {
      var maxWeekCountPerRow;
      var output = '<div class="ohe-cal-head">';
      output += '<input type="button" class="prev ohe-cal-nav ' + settings.nav_classes + '" value="Prev" data-year="' + (year - 1) + '">';
      output += '&nbsp; ' + year + ' &nbsp;';
      output += '<input type="button" class="next ohe-cal-nav ' + settings.nav_classes + '" value="Next" data-year="' + (year + 1) + '">';

      if (settings.profiles) {
        output += generateProfileOptionsForm();
      }
      output += '</div><div class="ohe-cal-rows">';
      var rows = Math.ceil(12 / settings.cols);
      for (var row = 0; row < rows; row++) {
        maxWeekCountPerRow = getMaxWeekCount(year, settings.cols * row, settings.cols * row + settings.cols);
        output = output + '<div class="ohe-cal-row cols' + settings.cols + '">';
        for (var col = 0; col < settings.cols; col++) {
          output += generateMonth(year, (settings.cols * row) + col, maxWeekCountPerRow);
        }
        output += '</div>';
      }
      output += '<div style="clear: both" /></div>';
      return output;
    }

    // Utility function to get weekcount for multiple months.
    function getMaxWeekCount(year, monthStart, monthEnd) {
      var max = 0;
      for (var i = monthStart; i <= monthEnd; i++) {
        max = Math.max(max, getWeekCount(year, i));
      }

      return max;
    }

    // How many weeks are prt of this month.
    function getWeekCount(year, month) {
      // Days in this month.
      var days = new Date(year, month + 1, 0).getDate();

      var daysInFirstWeek = getDaysInFirstWeek(year, month);

      var weeksWithoutFirstWeek = Math.ceil((days - daysInFirstWeek) / 7);

      return weeksWithoutFirstWeek + 1;
    }

    // Returns count of days which belongs to the first week of this month.
    function getDaysInFirstWeek(year, month) {
      // Get day of week for the first of given month. (0=so)
      var monthBeginDow = new Date(year, month, 1).getDay();

      var daysInFirstWeek = (7 - monthBeginDow + settings.first_dow) % 7;

      if (daysInFirstWeek === 0) {
        daysInFirstWeek = 7;
      }

      return daysInFirstWeek;
    }

    // Generates table-markup which represents a month.
    function generateMonth(year, month, weekRowsFillUpCount) {

      var weekdays = settings.weekdays_abbr.concat(settings.weekdays_abbr);

      // Days in this month.
      var days = new Date(year, month + 1, 0).getDate();
      // Day Counter.
      var day = 0;
      // Week Counter.
      var week = 0;
      // Day of week.
      // Possible confusion, because other values than 0-6 possible.
      // Can be anything between x and x+7.
      var dow;

      // Marker for first day for output.
      var dowStart = settings.first_dow + 7 - getDaysInFirstWeek(year, month);

      // Init output.
      var table = '<table data-month="' + month + '"><thead><tr><th colspan="7"><span class="cal-th">' + settings.months[month] + '</span></th></tr><tr>';
      for (dow = settings.first_dow; dow < (settings.first_dow + 7); dow++) {
        table = table + '<th>' + weekdays[dow] + '</th>';
      }
      table = table + '</tr></thead><tbody>';
      // Break on counter lower than days in month.
      while (day < days) {
        week++;
        // If there are days left, add new row.
        table = table + '<tr>';
        for (dow = settings.first_dow; dow < (settings.first_dow + 7); dow++) {

          if (((dow < dowStart) && (week === 1)) || day >= days) {
            // The day belongs to next or prev month.
            table = table + '<td class="ohe-cal-inactive"></td>';
          }
          else {
            day++;

            var classes = [];
            // Is the day already selected(in single dates)? ->set class.
            if (!$.isEmptyObject(singleDates) && !$.isEmptyObject(singleDates[year]) && !$.isEmptyObject(singleDates[year][month]) && singleDates[year][month].indexOf(day) >= 0) {
              classes.push('ohe-cal-selected');
            }
            // Is the day already selected(in profiles)? ->set class.
            if (!$.isEmptyObject(profileDates) && !$.isEmptyObject(profileDates[year]) && !$.isEmptyObject(profileDates[year][month]) && profileDates[year][month].indexOf(day) >= 0) {
              classes.push('ohe-cal-selected_by_profile');
            }
            table = table + '<td data-month="' + month + '" data-day="' + day + '" class="' + classes.join(' ') + '">' + day + '</td>';

          }

        }
        table = table + '</tr>';
      }

      // Fill up with empty cells.
      while (week < weekRowsFillUpCount) {
        week++;
        table = table + '<tr><td class="ohe-cal-inactive" colspan="7">&nbsp;</td></tr>';
      }

      table = table + '</tbody></table>';
      return table;
    }

    // Make me chainable.
    return this;
  };
}(jQuery));
