/**
 * @file
 * Timetable widget logic.
 */

(function ($) {
  "use strict";

  $.fn.timetable = function (options) {
    // Private vars.
    // This ist hier die collection.
    var $context = this;

    // Defaults.
    var settings = $.extend({
      data_container: ".ohe-timeslot-container",
      table_markup_container: ".ohe-timetable",
      switch_controls_container: ".ohe-switch-stat"
    }, options);

    init();

    function init() {

      var $data_container = $(settings.data_container, $context);

      // Init classes for selected timeslots.
      var initialTimeslotsByDay = JSON.parse($data_container.val());
      $('tr', $context).each(function () {
        var $tr = $(this);
        var dow = $tr.data('dow');
        var timeslots = initialTimeslotsByDay[dow];
        var inSequence = false;

        $('td', $tr).each(function (timeslotIndex) {

          if (inSequence !== true && timeslotIndex === timeslots[0]) {
            inSequence = true;
            timeslots.shift();
          }
          else if (inSequence === true && timeslotIndex === timeslots[0]) {
            inSequence = false;
            timeslots.shift();
          }
          if (inSequence === true) {
            $(this).addClass('ui-selected');
          }
          // Break on empty timeslots.
        });
      });

      var selectionStatus = true;
      var selectable = init_timetable_selectable($(settings.table_markup_container, $context), selectionStatus, $data_container);

      // Switching the add-remove state of the widget.
      $(settings.switch_controls_container + ' input', $context).click(function (e) {
        var link = $(this);

        // Set the modus class on the wrapper.
        if (link.hasClass('ohe-add')) {
          selectionStatus = true;
          $context.removeClass('ohe-remove').addClass('ohe-add');
        }
        else {
          selectionStatus = false;
          $context.removeClass('ohe-add').addClass('ohe-remove');
        }

        // Switch active class on the buttons.
        link.attr("disabled", "disabled").siblings().removeAttr('disabled');

        // Ui-selected klasse bleibt bestehen.
        selectable.selectable("destroy");
        // Rewrite (invert).
        $(settings.table_markup_container + " td", $context).toggleClass('ui-selected');
        // Reinitialize.
        selectable = init_timetable_selectable($(settings.table_markup_container, $context), selectionStatus, $data_container);

        e.preventDefault();
      });
    }

    function init_timetable_selectable(timetable, selectionStatus, $data_container) {
      return timetable.selectable({
        filter: "tr td:not(.ui-selected):not(:first-child)",
        stop: function (event, ui) {
          // Count classes by selection state.
          // Init container.
          var timeslotsByDay = [
            [],
            [],
            [],
            [],
            [],
            [],
            []
          ];

          $('tbody tr', $(this)).each(function () {
            var $tr = $(this);
            var dow = $tr.data('dow');
            var lastSelected;
            // Check the timeslots -> prüfen ob aufeinander folgende td's selected (nicht) gesetzt haben.
            // For correct sequence start.
            if (selectionStatus === true) {
              lastSelected = false;
            }
            else {
              lastSelected = true;
            }

            $('td', $tr).each(function (timeslotIndex) {

              if ($(this).hasClass('ui-selected') !== lastSelected) {
                // Change sequence.
                timeslotsByDay[dow].push(timeslotIndex);

                lastSelected = $(this).hasClass('ui-selected');
              }
            });

            // For correct sequence endings.
            if (timeslotsByDay[dow].length % 2 > 0) {
              timeslotsByDay[dow].push($('td', $tr).length);
            }
          });

          // Why not in the field format-> quickest way in js to store/restore.
          $data_container.val(JSON.stringify(timeslotsByDay));

        }
      });
    }

    // Make me chainable.
    return this;
  };

})(jQuery);
