/**
 * @file
 * Exception-cal init.
 */

(function ($) {
  "use strict";

  Drupal.behaviors.officeHourExtraExceptionCal = {
    attach: function ($context, settings) {
      $(".ohe-cal").each(function (index) {
        var settings = Drupal.settings.office_hours_extra[$(this).data('config')];
        $(this).exception_cal(settings);
      });
    }
  };

}(jQuery));
