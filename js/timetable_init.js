/**
 * @file
 * Timetable widget init.
 */

(function ($) {
  "use strict";

  Drupal.behaviors.officeHourExtra = {
    attach: function ($context, settings) {
      $(".ohe-timetable-wrapper").each(function (index) {
        $(this).timetable();
      });
    }
  };

})(jQuery);
