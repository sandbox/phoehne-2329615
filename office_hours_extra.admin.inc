<?php
/**
 * @file
 * Form for crud-profiles.
 */

/**
 * Implements Admin Form (Profiles).
 */
function office_hours_extra_profile_edit_form($form, &$form_state, $profile) {
  $new = TRUE;

  // Fetch dates from this profile.
  if (is_numeric($profile)) {
    $new = FALSE;

    $items = office_hours_extra_profile_get_dates($profile);

    $default_value_single_dates = array();

    foreach ($items as $profile_item) {
      // Single date.
      $single_date = new DateTime($profile_item->profile_date);
      $default_value_single_dates[$single_date->format('Y')][($single_date->format('n') - 1)][] = (int) $single_date->format('j');
    }

    $default_value_single_dates = json_encode($default_value_single_dates);
    // Double brackets escaped.
    $default_value_single_dates = str_replace('\\', '', $default_value_single_dates);
  }

  // Here only this widget can occur.
  $unique_widget_identifier = 'admin';

  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile-Name'),
    '#description' => t('e.a. Easter'),
    '#required' => TRUE,
    '#default_value' => $new ? '' : $profile_item->profile_name,
  );

  $form['pid'] = array(
    '#type' => 'value',
    '#value' => $profile,
  );

  $form['multidatespicker'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('ohe-cal'),
      'data-config' => $unique_widget_identifier,
    ),
  );
  $form['multidatespicker']['single_dates'] = array(
    '#type' => 'hidden',
    '#default_value' => $new ? '{}' : $default_value_single_dates,
    '#attributes' => array(
      'class' => array('ohe-cal-dates'),
    ),
  );

  // Prepare some defaults (eg localized descriptions).
  $exception_cal_defaults = array(
    'weekdays_abbr' => date_week_days_abbr(TRUE),
    'months' => array_values(date_month_names(TRUE)),
    'first_dow' => intval(variable_get('date_first_day', 1)),
    'profiles' => FALSE,
    'cols' => 4,
  );

  $form += array(
    '#attached' => array(
      // Add javascript to trigger the Calendar.
      'js' => array(
        array(
          'data' => array('office_hours_extra' => array($unique_widget_identifier => $exception_cal_defaults)),
          'type' => 'setting',
        ),
        drupal_get_path('module', 'office_hours_extra') . '/js/exception_cal_init.js',
      ),
      'library' => array(
        array('office_hours_extra', 'exception_cal'),
      ),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $new ? t('Add') : t('Submit'),
  );
  if (!$new) {
    $form['submit2'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('office_hours_extra_profile_edit_form_delete_submit'),
      '#limit_validation_errors' => array(array('pid')),
    );
  }

  return $form;
}

/**
 * Validate callback for profile form.
 */
function office_hours_extra_profile_edit_form_validate($form, &$form_state) {
  $empty = TRUE;
  $single_dates = json_decode($form_state['values']['single_dates']);

  foreach ($single_dates as $single_dates_by_month) {
    foreach ($single_dates_by_month as $single_dates_for_month) {
      if (is_array($single_dates_for_month) && !empty($single_dates_for_month)) {
        $empty = FALSE;
        break 2;
      }
    }
  }

  if ($empty) {
    form_set_error('multidatespicker', t('No dates selected'));
  }
}

/**
 * Submit callback for delete.
 */
function office_hours_extra_profile_edit_form_delete_submit($form, &$form_state) {
  db_delete('office_hours_extra_exception_profiles')->condition('pid', $form_state['values']['pid'])->execute();

  $form_state['redirect'] = 'admin/config/content/timetable';
}

/**
 * Submit callback for edit.
 */
function office_hours_extra_profile_edit_form_submit($form, &$form_state) {
  // Delete all old.
  if (is_numeric($form_state['values']['pid'])) {
    db_delete('office_hours_extra_exception_profiles')->condition('pid', $form_state['values']['pid'])->execute();
    $pid = $form_state['values']['pid'];
  }
  else {
    // New pid.
    $query = db_select('office_hours_extra_exception_profiles');
    $query->addExpression('MAX(pid)');
    $max = $query->execute()->fetchField();
    $pid = $max + 1;
  }

  $single_dates = json_decode($form_state['values']['single_dates']);

  foreach ($single_dates as $year => $single_dates_by_month) {
    foreach ($single_dates_by_month as $month => $single_dates_for_month) {
      foreach ($single_dates_for_month as $day) {
        $record = new stdClass();
        $record->profile_name = $form_state['values']['name'];
        $record->profile_date = $year . '-' . ($month + 1) . '-' . $day;
        $record->pid = $pid;
        drupal_write_record('office_hours_extra_exception_profiles', $record);
      }
    }
  }

  $form_state['redirect'] = 'admin/config/content/timetable';
}

/**
 * Build render array which renders to table with all profiles.
 *
 * @return array
 *   Returns Render Array.
 */
function office_hours_extra_list() {
  $rows = array();

  $query = db_select('office_hours_extra_exception_profiles', 'p');
  $query->groupBy('p.pid');
  // $query->addExpression('GROUP_CONCAT(p.profile_date)', 'all_dates');
  $query->fields('p', array('profile_name', 'pid'))->orderBy('pid', 'DESC');

  $result = $query->execute();

  foreach ($result as $entry) {
    // Sanitize the data before handing it off to the theme layer.
    $rows[] = array(
      check_plain($entry->profile_name),
      l(t('Edit'), 'admin/config/content/timetable/edit/' . $entry->pid),
    );
  }

  $table = array(
    '#theme' => 'table',
    '#header' => array(t('Profile Name'), t('Action')),
    '#rows' => $rows,
  );

  return $table;
}
