INTRODUCTION
------------

This Project extends the Office Hours Project by additional userfriendly widgets
and an additional field.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/phoehne/2329615

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/2329615

- new office_hours widget:
 - graphical office_hours timetable(no more thousends of dropdowns)
 - this is build with jquery selectable
- new datefield & widget
 - for selecting multiple exception dates from weekly timetable
 - widget is a yearly calendar
- an administration backend to define profiles for exception dates,
 - so you don't have to select all the christmas dates per hand.

USAGE
-----

_Timetable Widget_

Add a field of type office hours.
Besides the original widget you can now choose the timetable widget.
In this widget you have a matrix with all available timeslots,
as defined in your field settings. You can select single slots
by clicking it or drag a rectangle to select an area with slots.
There are two Buttons to change between adding and removing mode.
In removing-mode you can likwise click or drag to remove slots.

_Dates Exceptions Field and Widget_

This adds a new field type and a widget for this field.
Its intended for use as helper to organize special close-days for offices.
Because this is not possible within a weekly timetable.
The widget shows a yearly calendar where you can select days
by clicking and deselect by simply clicking again.
you can select longer periods by normal(left) click on day
and right click(or shift left click) another day after that.
Besides this direct selecting of days you can define some presets
like christmas or easter-hollydays in the date-api settings page.
these profiles you can activate (unhide this functionality
by click on profile button)
At the moment there is no formatter for these field,
because i personally need this only for internal calculations,
but its absolutely useful to build an advanced office hours
formatter which respects this close-days, or at least show
upcoming close days as a formatter. patches are welcome.


REQUIREMENTS
------------

This module requires the following modules:

 * Office Hours (https://www.drupal.org/project/office_hours)
 * jQuery Update (https://www.drupal.org/project/jquery_update)
    choose minimum v1.7

 Jquery Update is only necessary if you want to use the exception calendar
 field, for the timetable widget stock jquery is sufficient.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administrate Timetable Settings

     CRUD Functionality for the profiles (see below)

 * The exception calendar can optional used with profiles.
   These profiles integrates in the dates_api settings page
   admin/config/date/timetable

 * Besides the latter there is nothing special.
   Configuration works like every other field/widget/formatter


MAINTAINERS
-----------

Current maintainers:
 * Peter Höhne (phoehne) - https://drupal.org/user/379745
